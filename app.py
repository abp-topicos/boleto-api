import re
import time
from io import BytesIO

import pika
import qrcode
import json
import os
from flask import Flask, request, jsonify, send_file

from config.database import Database
from models.boleto import db, Boleto
from dotenv import load_dotenv
from flask_swagger_ui import get_swaggerui_blueprint

app = Flask(__name__)
app.config.from_object(Database)
db.init_app(app)
load_dotenv()

RABBITMQ_URL = os.getenv('RABBITMQ_URL')

with app.app_context():
    db.create_all()


@app.route('/boleto', methods=['POST'])
def create_boleto():
    data = request.json
    cod_boleto = generate_cod_boleto(data['valor'])

    boleto = Boleto(cod_boleto=cod_boleto, nome_pessoa=data['pessoa']['nome'],
                    email_pessoa=data['pessoa']['email'], valor=data['valor'])

    db.session.add(boleto)
    db.session.commit()

    return jsonify({"message": "Boleto cadastrado com sucesso!", "id": boleto.id}), 201


@app.route('/boleto/<int:boleto_id>/gerar', methods=['GET'])
def generate_boleto(boleto_id):
    boleto = Boleto.query.get_or_404(boleto_id)
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=1,
    )
    qr.add_data(f"http://localhost:5000/boleto/{boleto.id}/pagar")
    qr.make(fit=True)
    img = qr.make_image(fill='black', back_color='white')

    img_io = BytesIO()
    img.save(img_io, 'PNG')
    img_io.seek(0)

    return send_file(img_io, mimetype='image/png')


@app.route('/boleto/<int:boleto_id>/pagar', methods=['GET'])
def pay_boleto(boleto_id):
    boleto = Boleto.query.get_or_404(boleto_id)

    message = {
        "id": boleto.id,
        "nome_pessoa": boleto.nome_pessoa,
        "email_pessoa": boleto.email_pessoa
    }

    send_message_to_compensacao(message)

    return jsonify({"message": "Boleto está aguardando o processo de compensação"})


@app.route('/boleto/<int:boleto_id>/compensar', methods=['POST'])
def change_situacao_to_compensado(boleto_id):
    boleto = Boleto.query.get_or_404(boleto_id)
    boleto.situacao = 'COMPENSADO'
    db.session.commit()

    return jsonify({"message": "Boleto compensado com sucesso!"}), 201


@app.route("/swagger.json")
def spec():
    swagger_json = os.path.join('swagger', 'swagger.json')

    with open(swagger_json, 'r') as file:
        swagger_json_compact = json.load(file)

    return jsonify(swagger_json_compact)


def setup_swagger_route(app):
    SWAGGER_URL = '/docs'
    API_URL = '/swagger.json'

    swaggerui_blueprint = get_swaggerui_blueprint(
        SWAGGER_URL,
        API_URL,
        config={
            'app_name': "Boleto API"
        }
    )

    app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)


def send_message_to_compensacao(message):
    params = pika.URLParameters(RABBITMQ_URL)
    connection = pika.BlockingConnection(params)
    channel = connection.channel()
    queue_name = 'compensacao'
    channel.queue_declare(queue=queue_name, durable=True)

    channel.basic_publish(
        exchange='',
        routing_key=queue_name,
        body=json.dumps(message),
        properties=pika.BasicProperties(
            delivery_mode=2,
        )
    )

    connection.close()


def generate_cod_boleto(valor):
    valor_str = re.sub(r'\D', '', str(valor))
    return str(time.time()).replace('.', '') + valor_str


setup_swagger_route(app)

if __name__ == '__main__':
    app.run(debug=True)
