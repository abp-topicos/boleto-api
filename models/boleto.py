from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Boleto(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cod_boleto = db.Column(db.String(100), nullable=False)
    nome_pessoa = db.Column(db.String(100), nullable=False)
    email_pessoa = db.Column(db.String(100), nullable=False)
    valor = db.Column(db.Float, nullable=False)
    situacao = db.Column(db.String(20), nullable=False, default='AGUARDANDO_PAGAMENTO')
