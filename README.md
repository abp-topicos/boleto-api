# Documentação da API
Esta API fornece funcionalidades para a criação de boletos e geração de QR codes para pagamento.

## Requisitos
- Python >= 3.6

## Swagger
Acesse o endereço abaixo para visualizar a documentação interativa do Swagger:
```bash
http://localhost:5000/docs
```

## Rodar o Projeto

### 1. Instalar as Dependências
Execute o seguinte comando para instalar todas as dependências necessárias listadas no arquivo requirements.txt:
```sh
pip3 install -r requirements.txt
```

### 2. Iniciar a Aplicação
Para iniciar a aplicação, execute o seguinte comando:
```bash
python3 app.py
```
Após o processo acima, o projeto estará rodando na porta 5000